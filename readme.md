## LARAVEL REST API for Game

[POST]      create new admin  http://localhost:8000/api/user/register

[POST]      login as admin to get token  http://localhost:8000/api/user/login

[GET]		list games http://localhost:8000/api/

[GET]		show game detail : http://localhost:8000/api/games/{id}

[POST]		create new game http://localhost:8000/api/games

[PUT]		update game http://localhost:8000/api/games/{id}

[DELETE]	delete game http://localhost:8000/api//games/{id}


[GET]		list player	http://localhost:8000/api/player

[GET]		detail player http://localhost:8000/api/player/{id}

[POST]		create new player http://localhost:8000/api/player

[PUT]		update player http://localhost:8000/api/player/{id}

[DELETE]	delete player http://localhost:8000/api/player/{id}

[POST]		add player point http://localhost:8000/api/point/plus/{player_id}/{game_id} 

[POST]		min player point http://localhost:8000/api/point/minus/{player_id}/{game_id} 

[GET]		show leaderboard on game http://localhost:8000/api/leader_board/{game_id}
