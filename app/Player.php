<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class player extends Model
{
    protected $fillable = [
        'full_name', 'game_id', 'point',
    ];
}
