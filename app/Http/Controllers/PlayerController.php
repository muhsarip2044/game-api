<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Player;
use Validator;


class PlayerController extends Controller
{
    public function index()
    {
        return Player::all();
    }

    public function show($id)
    {
        return Player::find($id);
    }

    public function store(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'full_name' => 'required|string|max:255',
            'game_id'=> 'required|integer'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(),400);
        }

        $player = Player::create($request->all());

        return response()->json($player, 201);
    }

    public function update(Request $request, $id)
    {
    	$validator = Validator::make($request->all(), [
            'full_name' => 'required|string|max:255',
            'game_id'=> 'required|integer'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(),400);
        }

    	$player = Player::findOrFail($id);
        $player->update($request->all());

        return response()->json($player, 200);
    }

    public function delete(Request $request, $id)
    {
        $player = Player::findOrFail($id);
        $player->delete();

        return response()->json(null, 204);
    }
}
