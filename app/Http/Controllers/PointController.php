<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Player;
use App\Game;
use Validator;

class PointController extends Controller
{
    public function plus(Request $request,$player_id,$game_id)
    {
    	$player  = Player::where("id",$player_id)->where("game_id",$game_id)->first();
    	if ($player) {
	    	$player->point = ($player->point==null?0:$player->point) + $request->point;
	    	$player->save();

	    	return response()->json($player, 200); 
    	}else{
    		return response()->json(null, 400); 
    	}
    	 
    }

    public function minus(Request $request,$player_id,$game_id)
    {
    	$player  = Player::where("id",$player_id)->where("game_id",$game_id)->first();
    	if ($player) {
	    	$player->point = ($player->point==null?0:$player->point) - $request->point;
	    	$player->save();

	    	return response()->json($player, 200); 
    	}else{
    		return response()->json(null, 400); 
    	}
    	
    }
    public function leaderBoard($game_id=null)
    {
        if ($game_id!=null) {
            $game = Game::find($game_id);
            if ($game) {
                $leader_board = Player::
                    select("full_name","point")
                    ->where("game_id",$game_id)
                    ->orderBy("point","desc")
                    ->get();
                return response()->json(
                    [
                        "game" => $game,
                        "leader_biard"=> $leader_board
                    ], 
                200);
            }else{
                return response()->json(null, 400);
            }
        }else{
            return response()->json(null, 400);
        }
        
    }
}
