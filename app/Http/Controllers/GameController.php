<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Game;
use Validator;

class GameController extends Controller
{
    public function index()
    {
        return Game::all();
    }

    public function show($id)
    {
        return Game::find($id);
    }

    public function store(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'name' => 'required|string|unique:games|max:255',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(),400);
        }

        $game = Game::create($request->all());

        return response()->json($game, 201);
    }

    public function update(Request $request, $id)
    {
    	$validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(),400);
        }

    	$game = Game::findOrFail($id);
        $game->update($request->all());

        return response()->json($game, 200);
    }

    public function delete(Request $request, $id)
    {
        $game = Game::findOrFail($id);
        $game->delete();

        return response()->json(null, 204);
    }
}
