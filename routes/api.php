<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('user/register', 'APIRegisterController@register');
Route::post('user/login', 'APILoginController@login')->name('login');


Route::middleware('jwt.auth')->get('users', function(Request $request) {
    return auth()->user();
});

Route::group(['middleware' => ['jwt.auth']], function () {
	Route::get('games', 'GameController@index');
	Route::get('games/{id}', 'GameController@show');
	Route::post('games', 'GameController@store');
	Route::put('games/{id}', 'GameController@update');
	Route::delete('/games/{id}', 'GameController@delete'); 

	Route::get('player', 'PlayerController@index');
	Route::get('player/{id}', 'PlayerController@show');
	Route::post('player', 'PlayerController@store');
	Route::put('player/{id}', 'PlayerController@update');
	Route::delete('player/{id}', 'PlayerController@delete'); 

	Route::put('point/plus/{player_id}/{game_id}', 'PointController@plus'); 
	Route::put('point/minus/{player_id}/{game_id}', 'PointController@minus'); 

	Route::get('leader_board/{game_id}', 'PointController@leaderBoard');
});
